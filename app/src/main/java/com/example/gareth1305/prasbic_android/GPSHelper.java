package com.example.gareth1305.prasbic_android;

/**
 * Created by Gareth1305 on 23/11/2015.
 */
import android.location.LocationListener;
import android.location.LocationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
public class GPSHelper extends Service implements LocationListener {
    private final Context context;
    // flag for GPS status
    boolean isGPSEnabled = false;
    Location location;
    double latitude;
    double longitude;
    // minimum distance to update location in meters
    private static final long MIN_DISTANCE_CHANGE_TO_UPDATES = 5; // 5m
    // minimum time between updates in milliseconds
    private static final long MIN_TIME_ELAPSED_BETWEEN_UPDATES = 1000 * 60 * 1; // 1 minute
    // declarea Location Manager
    protected LocationManager locationManager;
    public GPSHelper(Context context) {
        this.context = context;
        getLocation();
    }
    public Location getLocation() {
        try {
            // get a reference to the system Location Manager service
            locationManager = (LocationManager) context
                    .getSystemService(LOCATION_SERVICE);
            // get GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            // if GPS is enabled get location via GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_DISTANCE_CHANGE_TO_UPDATES,
                            MIN_TIME_ELAPSED_BETWEEN_UPDATES, this);
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
        return latitude;
    }
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
        return longitude;
    }
    @Override
    public void onLocationChanged(Location location) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
    @Override
    public void onProviderEnabled(String provider) {
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}

