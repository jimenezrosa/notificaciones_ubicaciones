package com.example.gareth1305.prasbic_android;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.gareth1305.prasbic_android.entity.Report;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ViewReportsActivity extends HomePageActivity implements OnMapReadyCallback {

    Button viewDetailsReportsBtn, submitReportBtn;
    private GoogleMap mMap;
    List<Report> reports;
    LatLng myCurrentLocation;

    public void setMyCurrentLocation(LatLng myCurrentLocation) {
        this.myCurrentLocation = myCurrentLocation;
    }

    public LatLng getMyCurrentLocation() {
        return myCurrentLocation;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_reports);

        new GetReportDetails().execute("http://192.168.43.163:8080/restreports");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        viewDetailsReportsBtn = (Button) findViewById(R.id.viewReportsListBtn);
        viewDetailsReportsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((reports.size() == 0) || (reports == null)) {
                    Toast.makeText(getApplicationContext(),
                            "There are no reports to view",
                            Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(ViewReportsActivity.this,
                            ViewDetailedReports.class);
                    intent.putExtra("Report_List", (Serializable) reports);
                    startActivity(intent);
                }
            }
        });

        submitReportBtn = (Button) findViewById(R.id.submitASBOReport);
        submitReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewReportsActivity.this,
                        SubmitAsboReport.class);
                intent.putExtra("Lat_Val", getMyCurrentLocation().latitude);
                intent.putExtra("Lng_Val", getMyCurrentLocation().longitude);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
       // mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location myLocation = locationManager.getLastKnownLocation(provider);
        double latitude,longitude;
        latitude = myLocation.getLatitude();
        longitude = myLocation.getLongitude();
//        latitude = 53.2345678;
//        longitude = -6.23456;

        myCurrentLocation = new LatLng(latitude, longitude);
        setMyCurrentLocation(myCurrentLocation);
        for (Report r : reports) {
            LatLng reportLocation = new LatLng(r.getReportLatitude(), r.getReportLongitude());
            mMap.addMarker(new MarkerOptions().position(reportLocation).title(r.getReportCategory()));
        }
        mMap.addMarker(new MarkerOptions().position(myCurrentLocation).title("You are here!")
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        mMap.addCircle(new CircleOptions()
                .center(myCurrentLocation)
                .radius(200)
                .strokeColor(Color.parseColor("#ff8466"))
                .fillColor(Color.TRANSPARENT));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myCurrentLocation, 16));
    }

    public class GetReportDetails extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            reports = new ArrayList<>();
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(5000);
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                String finalJson = buffer.toString();
                JSONArray jsonArray = new JSONArray(finalJson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int reportId = jsonObject.getInt("id");
                    double reportLatitude = jsonObject.getDouble("reportLatitude");
                    double reportLongitude = jsonObject.getDouble("reportLongitude");
                    String reportCategory = jsonObject.getString("reportCategory");
                    String reporteeUserEmail = jsonObject.getString("reporteeUserEmail");
                    Report report = new Report(reportId, reportLatitude, reportLongitude,
                            reportCategory, reporteeUserEmail);
                    reports.add(report);
                }
                System.out.println("List Size1... " + reports.size());
                for (Report report : reports) {
                    System.out.println(report.toString());
                }
                return buffer.toString();

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
