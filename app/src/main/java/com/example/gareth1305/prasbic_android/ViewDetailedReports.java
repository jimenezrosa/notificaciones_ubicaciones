package com.example.gareth1305.prasbic_android;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.gareth1305.prasbic_android.entity.Report;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ViewDetailedReports extends HomePageActivity {
    List<Report> reports;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_detailed_reports);
        Intent i = getIntent();
        reports = (List<Report>) i.getSerializableExtra("Report_List");

        populateListView();
    }

    private void populateListView() {
        ArrayAdapter<Report> adapter = new MyListAdapter();
        ListView list = (ListView) findViewById(R.id.usersListView);
        list.setAdapter(adapter);
    }

    private class MyListAdapter extends ArrayAdapter<Report> {
        public MyListAdapter() {
            super(ViewDetailedReports.this, R.layout.single_report_view, reports);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Make sure we have a view to work with as we might have  been given null
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.single_report_view, parent, false);
            }
            // Find the car to work with
            Report currentReport = reports.get(position);

            // Reportee Email
            TextView reporteeUserEmail = (TextView) itemView.findViewById(R.id.reporteeEditTxtView);
            reporteeUserEmail.setText("" + currentReport.getReporteeUserEmail());

            // Report Location
            TextView reportLocation = (TextView) itemView.findViewById(R.id.reportLocEditTxtView);
            String location = calculateAddress(currentReport.getReportLatitude(), currentReport.getReportLongitude());
            reportLocation.setText(location);

            // Report Category
            TextView reportCategory = (TextView) itemView.findViewById(R.id.reportCategoryEditTxtView);
            reportCategory.setText(currentReport.getReportCategory());

            return itemView;
        }
    }

    public String calculateAddress(double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String formattedAddress = address + ", " + city;
            return formattedAddress;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Address Not Found";
    }
}
