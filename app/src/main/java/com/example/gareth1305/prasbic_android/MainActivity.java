package com.example.gareth1305.prasbic_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gareth1305.prasbic_android.entity.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button loginBtn, signinBtn;
    TextView emailTxtView, passwordTxtView;
    private GestureDetectorCompat gestureDetectorCompat;

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gestureDetectorCompat = new GestureDetectorCompat(this, new MyGestureListener());

        emailTxtView = (TextView) findViewById(R.id.email);
        passwordTxtView = (TextView) findViewById(R.id.password);

        emailTxtView.addTextChangedListener(textWatcher);
        passwordTxtView.addTextChangedListener(textWatcher);
        checkFieldsForEmptyValues();

        loginBtn.setEnabled(false);
        loginBtn = (Button) findViewById(R.id.login);
        loginBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailToCheck, passwordToCheck;
                emailToCheck = emailTxtView.getText().toString();
                passwordToCheck = passwordTxtView.getText().toString();
                new ValidateLoginAsyncListener().execute("http://192.168.43.163:8080/restusers", emailToCheck, passwordToCheck);
            }
        });
        signinBtn = (Button) findViewById(R.id.signup);
        signinBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            if(event2.getX() < event1.getX()){
                Intent intent = new Intent(
                        MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
            return true;
        }
    }

    private void checkFieldsForEmptyValues() {
        loginBtn = (Button) findViewById(R.id.login);
        String email = emailTxtView.getText().toString();
        String pass = passwordTxtView.getText().toString();

        if (email.length() > 0 && pass.length() > 0) {
            loginBtn.setEnabled(true);
        } else {
            loginBtn.setEnabled(false);
        }

    }

    public class ValidateLoginAsyncListener extends AsyncTask<String, String, String> {

        boolean success = false;
        private ArrayList<User> users;

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String emailToLookFor = params[1];
            String passwordToLookFor = params[2];
            Log.d("PARAMS", params[0]);
            Log.d("PARAMS", emailToLookFor);
            Log.d("PARAMS", passwordToLookFor);

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(5000);
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                users = new ArrayList<User>();
                String finalJson = buffer.toString();
                JSONArray jsonArray = new JSONArray(finalJson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.getInt("id");
                    String email = jsonObject.getString("email");
                    String firstName = jsonObject.getString("firstName");
                    String lastName = jsonObject.getString("lastName");
                    String password = jsonObject.getString("password");
                    String emergencyContactNum = jsonObject.getString("emergencyContactNum");
                    User user = new User(id, email, firstName, lastName, password, emergencyContactNum);
                    users.add(user);
                }
                for (User u : users) {
                    if (u.getEmail().equalsIgnoreCase(emailToLookFor) &&
                            u.getPassword().equals(passwordToLookFor)) {
                        SharedPreferences preferences = getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("EMAIL", u.getEmail());
                        editor.putString("FIRSTNAME", u.getFirstName());
                        editor.putString("LASTNAME", u.getLastName());
                        editor.putString("PASSWORD", u.getPassword());
                        editor.putString("EMERGENCYCONTACT", u.getEmergencyContactNum());
                        editor.commit();
                        success = true;
                        break;
                    }
                }
                return buffer.toString();

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (success) {
                Intent intent = new Intent(MainActivity.this,
                        HomePageActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Invalid Email or Password",
                        Toast.LENGTH_LONG).show();
            }
        }


    }
}
